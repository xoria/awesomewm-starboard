local awful = require("awful")

menu_awesome = {
  { "hotkeys",     function() return false, hotkeys_popup.show_help end},
  { "restart",     awesome.restart                },
  { "quit",        awesome.quit                   }
}

menu_system = {
  { "lock",           "slock"                  },
  { "suspend",        "systemctl suspend"      },
  { "lock & suspend", function () awful.spawn.with_shell("slock systemctl suspend -i") end },
--  { "hybrid-sleep",   "systemctl hybrid-sleep" },
--  { "hibernate",      "systemctl hibernate"    },
  {},
  { "reboot",         "systemctl reboot"       },
  { "poweroff",       "systemctl poweroff"     }
}

menu_main = {
  { "awesome",       menu_awesome },
  { "open terminal", terminal     },
  { "system",        menu_system  }
}

if launcher then
  menu = awful.menu({
    items = menu_main
  })
end

