local awful = require("awful")
local beautiful = require("beautiful")
local keys = require("utils/keys")
local keybindings = require("core/keybindings")

-- keybindings that are applied on a per-client basis rather than global
local clientkeys = awful.util.table.join(
  -- standard bindings (e.g. minimize, etc.)
  keybindings.clientkeys,
  -- custom bindings (defined within customize/keybindings.lua)
  clientkeys_custom
)

-- per-client mouse bindings
local clientbuttons = awful.util.table.join(
  -- mouse click => focus
  awful.button({      }, 1, function (c) client.focus = c; c:raise() end),
  -- super + mouse drag => move
  awful.button({ keys.super }, 1, awful.mouse.client.move),
  -- super + mouse right-button drag => resize
  awful.button({ keys.super }, 3, awful.mouse.client.resize)
)

local matchall_rule = {
  {
    -- All clients will match this rule.
    rule = { },
    properties = {
      -- apply theming
      border_width = beautiful.border_width,
      border_color = beautiful.border_normal,
      -- normalize
      focus = awful.client.focus.filter,
      raise = true,
      screen = awful.screen.preferred, -- the current screen
      fullscreen = false,
      maximized = false,
      maximized_horizontal = false,
      maximized_vertical = false,
      placement = awful.placement.no_overlap+awful.placement.no_offscreen,
      -- apply handlers
      keys = clientkeys,
      buttons = clientbuttons,
    },
  }
}

return matchall_rule

