local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")

local separator = wibox.widget.imagebox(beautiful.widget_sep)

awful.screen.connect_for_each_screen(function(s)
  local wibar = awful.wibar({ screen = s, position = "top" })
  promptbox[s] = require("wiboxes/components/prompt")()

  -- Create left layout
  local layout_left = wibox.layout.fixed.horizontal()
  require("wiboxes/components/launcher")(layout_left)
  layout_left:add(separator)
  require("wiboxes/components/datetime")(layout_left, s, "tl")
  layout_left:add(separator)
  require("wiboxes/components/tags")(layout_left, s)
  layout_left:add(promptbox[s])
  layout_left:add(separator)

  -- Create right layout
  local layout_right = wibox.layout.fixed.horizontal()
  layout_right:add(separator)
  if traffic_device then
    require("wiboxes/components/traffic")(layout_right)
    layout_right:add(separator)
  end
  if s.index == 1 then -- systray not working for multiple screens
    layout_right:add(wibox.widget.systray())
    layout_right:add(separator)
  end
  if memory then
    require("wiboxes/components/memory")(layout_right)
    layout_right:add(separator)
  end
  if cpu_thermal then
    require("wiboxes/components/cpu-temp")(layout_right)
    layout_right:add(separator)
  end
  if battery then
    require("wiboxes/components/battery")(layout_right)
    layout_right:add(separator)
  end
  require("wiboxes/components/volume")(layout_right)
  layout_right:add(separator)
  require("wiboxes/components/layout")(layout_right, s)

  -- Combine all layouts
  local layout = wibox.layout.align.horizontal()
  layout:set_first(layout_left)
  layout:set_second(require("wiboxes/components/tasks")(nil, s))
  layout:set_third(layout_right)

  -- Apply layout
  wibar:set_widget(layout)
end)
