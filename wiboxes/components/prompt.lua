local awful = require("awful")

local function create()
  return awful.widget.prompt()
end

return create

