local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local vicious = require("vicious")

local function update_volume(widget)
  local volume_stream = io.popen(bin_dir .. "/volume p")
  local mute_stream = io.popen(bin_dir .. "/volume pm")
  local volume_str = volume_stream:read("*all")
  local mute_str = mute_stream:read("*all")

  volume_stream:close()
  mute_stream:close()

  local volume = tonumber(volume_str)

  if volume then
    if volume < 0 then volume = 0 end
    local volume_label
    local is_mute = not string.find(mute_str, "no", 1, true)
    local bg_color = 'white'

    if is_mute then
      bg_color = '#666666'
    elseif volume > 100 then
      bg_color = '#ffbbbb'
    end

    volume_label = "<span color='black' background='" .. bg_color .. "'><small> </small>"
      .. string.format("%3d", volume)
      .. "%<small> </small></span>"

    widget:set_markup(volume_label)
  end
end

local function register(layout)
  widget = wibox.widget.textbox()

  update_volume(widget)

  timer = gears.timer({ timeout = 2 })
  timer:connect_signal("timeout", function () update_volume(widget) end)
  timer:start()
  
  widget:buttons(awful.util.table.join(
    awful.button({}, 1,
      function ()
        awful.spawn(bin_dir .. "/volume m")
        update_volume(widget)
      end
    ),
    awful.button({}, 4,
      function ()
        awful.spawn(bin_dir .. "/volume +")
        update_volume(widget)
      end
    ),
    awful.button({}, 5,
      function ()
        awful.spawn(bin_dir .. "/volume -")
        update_volume(widget)
      end
    )
  ))

  layout:add(widget)

  return widget
end

return register

