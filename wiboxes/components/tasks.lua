local awful = require("awful")

local buttons = awful.util.table.join(
  awful.button({ }, 1,
    function (c)
      if c == client.focus then
        c.minimized = true
      else
        -- Without this, the following c:isvisible() makes no sense
        c.minimized = false
        if not c:isvisible() then
          awful.tag.viewonly(c:tags()[1])
        end
        -- This will also un-minimize the client, if needed
        client.focus = c
        c:raise()
      end
    end
  ),
  awful.button({ }, 2, function (c) c:kill() end),
  awful.button({ }, 3,
    function (c)
      if instance then
        instance:hide()
        instance = nil
      else
        instance = awful.menu.clients({ width=250 })
      end
    end
  ),
  awful.button({ }, 4,
    function (c)
      awful.client.focus.byidx(1)
      if client.focus then client.focus:raise() end
    end
  ),
  awful.button({ }, 5,
    function (c)
      awful.client.focus.byidx(-1)
      if client.focus then client.focus:raise() end
    end
  )
)

local function register(layout, screen)
  local widget = awful.widget.tasklist(screen, awful.widget.tasklist.filter.currenttags, buttons)

  if layout then
    layout:set_middle(widget)
  end

  return widget
end

return register

