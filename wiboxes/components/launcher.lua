local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")

local function register(layout, screen)
  local widget
  if launcher then
    widget = awful.widget.launcher({ image = beautiful.awesome_icon, menu = menu })
  else
    widget = wibox.widget.imagebox(beautiful.awesome_icon)
  end
  
  layout:add(widget)
end

return register

