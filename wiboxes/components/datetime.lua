local awful = require("awful")
local vicious = require("vicious")
local wibox = require("wibox")

local function register(layout, screen, position, format)
  widget = wibox.widget.textbox()
  if not format then
    format = "%a  %m/%d  %R"
  end
  vicious.register(widget, vicious.widgets.date, format, 1)

  layout:add(widget)

  local calendar_widget_month = awful.widget.calendar_popup.month({ week_numbers = true })
  local calendar_widget_year = awful.widget.calendar_popup.year({ week_numbers = true })
  calendar_widget_month:attach(widget, position)
  widget:buttons(
    awful.button({ }, 1, function () calendar_widget_year:toggle() end)
  )

  calendars[screen] = calendar_widget_year

  return widget
end

return register

