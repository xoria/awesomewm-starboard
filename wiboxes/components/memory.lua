local beautiful = require("beautiful")
local wibox = require("wibox")
local vicious = require("vicious")

local function register(layout)
  local icon = wibox.widget.imagebox()
  icon:set_image(beautiful.widget_mem)
  local widget = wibox.widget.textbox()
  vicious.register(widget, vicious.widgets.mem, " $1%  ( $2<small> </small>MB ) ", 15)

  layout:add(icon)
  layout:add(widget)
end

return register

