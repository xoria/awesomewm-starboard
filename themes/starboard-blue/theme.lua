theme = {}
local beautiful = require("beautiful")

local util = require('awful.util')
local theme_path = beautiful.theme_path

theme.font          = "sans 8"

theme.bg_normal     = "#0A0A0A"
theme.bg_focus      = "#00203C"
theme.bg_urgent     = "#32528A"
theme.bg_minimize   = "#100000"

theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#bbbbbb"
theme.fg_focus      = "#eeeeff"
theme.fg_urgent     = "#aaaacc"
theme.fg_minimize   = "#4b4b4b"
theme.fg_highlight  = "#40607C"

theme.border_width  = 2
theme.border_normal = "#000020"
theme.border_focus  = "#00586A"
theme.border_marked = "#1c2391"

theme.fg_widget        = "#AECF96"
theme.fg_center_widget = "#88A175"
theme.fg_end_widget    = "#FF5656"
theme.fg_off_widget    = "#494B4F"
theme.fg_netup_widget  = "#7F9F7F"
theme.fg_netdn_widget  = theme.fg_urgent
theme.bg_widget        = theme.bg_normal
theme.border_widget    = theme.bg_normal

theme.widget_cpu    = theme_path .. "/icons/cpu.png"
theme.widget_bat    = theme_path .. "/icons/bat.png"
theme.widget_mem    = theme_path .. "/icons/mem.png"
theme.widget_fs     = theme_path .. "/icons/disk.png"
theme.widget_net    = theme_path .. "/icons/down.png"
theme.widget_netup  = theme_path .. "/icons/up.png"
theme.widget_wifi   = theme_path .. "/icons/wifi.png"
theme.widget_mail   = theme_path .. "/icons/mail.png"
theme.widget_vol    = theme_path .. "/icons/vol.png"
theme.widget_org    = theme_path .. "/icons/cal.png"
theme.widget_date   = theme_path .. "/icons/time.png"
theme.widget_crypto = theme_path .. "/icons/crypto.png"
theme.widget_sep    = theme_path .. "/icons-starboard/separator.png"

theme.taglist_squares_sel   = theme_path .. "/icons/taglist/squarefw.png"
theme.taglist_squares_unsel = theme_path .. "/icons/taglist/squarew.png"

theme.menu_submenu_icon = theme_path .. "/icons/submenu.png"
theme.menu_height = 15
theme.menu_width  = 100

theme.wibox_height = 21

theme.titlebar_close_button_normal = theme_path .. "/icons/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme_path .. "/icons/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme_path .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = theme_path .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme_path .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = theme_path .. "/icons/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = theme_path .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = theme_path .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme_path .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = theme_path .. "/icons/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = theme_path .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = theme_path .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme_path .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = theme_path .. "/icons/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = theme_path .. "/icons/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme_path .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme_path .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = theme_path .. "/icons/titlebar/maximized_focus_active.png"

theme.wallpaper = theme_path .. "/wall.jpg"
theme.wallpaper_color = "#000000"

theme.hotkeys_modifiers_fg = theme.fg_highlight
theme.hotkeys_label_bg = theme.bg_focus
theme.hotkeys_border_width = 0

theme.layout_fairh = theme_path .. "/icons-starboard/layouts/fairhw.png"
theme.layout_fairv = theme_path .. "/icons-starboard/layouts/fairvw.png"
theme.layout_floating  = theme_path .. "/icons-starboard/layouts/floatingw.png"
theme.layout_magnifier = theme_path .. "/icons-starboard/layouts/magnifierw.png"
theme.layout_max = theme_path .. "/icons-starboard/layouts/maxw.png"
theme.layout_fullscreen = theme_path .. "/icons-starboard/layouts/fullscreenw.png"
theme.layout_tilebottom = theme_path .. "/icons-starboard/layouts/tilebottomw.png"
theme.layout_tileleft   = theme_path .. "/icons-starboard/layouts/tileleftw.png"
theme.layout_tile = theme_path .. "/icons-starboard/layouts/tilew.png"
theme.layout_tiletop = theme_path .. "/icons-starboard/layouts/tiletopw.png"
theme.layout_spiral  = theme_path .. "/icons-starboard/layouts/spiralw.png"
theme.layout_dwindle = theme_path .. "/icons-starboard/layouts/dwindlew.png"

theme.calendar_normal_bg_color = "#2a2a2a"
theme.calendar_header_bg_color = "#94a0af"
theme.calendar_header_fg_color = theme.bg_normal
theme.calendar_yearheader_bg_color = theme.bg_focus
theme.calendar_yearheader_border_width = 0
theme.calendar_header_border_width = 0
theme.calendar_year_border_width = 0
theme.calendar_month_border_width = 0
theme.calendar_weekday_border_width = 0
theme.calendar_weeknumber_border_width = 0
theme.calendar_normal_border_width = 0
theme.calendar_focus_border_width = 0

theme.awesome_icon = theme_path .. "/icons/wheel.png"

theme.icon_theme = nil

return theme

